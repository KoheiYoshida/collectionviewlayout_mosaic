//
//  CollectionViewLayout_Mosaic.swift
//  CollectionViewLayout_Mosaic
//
//  Created by kohei yoshida on 2020/10/22.
//

import Foundation
import UIKit


class CollectionViewLayout_Mosaic: UICollectionViewFlowLayout{

    private var contentBounds = CGRect.zero
    // MARK:- セルのレイアウト情報を格納する
    private var cachedAttributes = [UICollectionViewLayoutAttributes]()
    
//    private let viewModel = 必要に応じてデータモデルから値を取得
        
    // MARK:- CollectionViewのCGSizeを返し、スクロール量を決める
    // Tips:- デフォルトではCGSize(0,0)が返される
    override var collectionViewContentSize: CGSize {

        return contentBounds.size
    }
    

    
    
    // Tips:- コールされるタイミング　：　レイアウト初回作成時、レイアウト変更時、invalidateLayout()が呼ばれたとき
    override func prepare() {
        super.prepare()
        
        guard let cv = collectionView else {return}
        cv.contentSize = collectionViewContentSize
        cv.isScrollEnabled = false
        
        // 現在までのレイアウトをキャンセル
        cachedAttributes.removeAll()
        contentBounds = CGRect(origin: .zero, size: cv.bounds.size)
        
        // TODO:- アイテム（セル）に対して
        // attributesを準備
        // 作成したattributesを、cachedAttributesに格納
        // contentBoundsとattribues.frameを一致する
        let count = cv.numberOfItems(inSection: 0)
        
        var currentIndex = 0
        var lastFrame: CGRect = .zero
        
        let cvWidth = cv.bounds.size.width
        let cvHeight = cv.bounds.size.height
        
        // インデックスは、count -1 なので　< でOK
        // アイテムのレイアウトを、一つずつ決定していく
        while currentIndex < count {
            let segmentFrame = CGRect(x: 0, y: lastFrame.maxY + 1.0, width: cvWidth, height: cvHeight)
            let layouts = MosaicLayoutProperty(frameRect: segmentFrame, itemCount: count)
            
            if layouts.segmentRects != []{
                for rect in layouts.segmentRects{
                    let attr = UICollectionViewLayoutAttributes(forCellWith: IndexPath(item: currentIndex, section: 0))
                    attr.frame = rect
                    
                    cachedAttributes.append(attr)
                    contentBounds = contentBounds.union(lastFrame)
                    
                    // currentIndex >= count になったらbreak
                    currentIndex += 1
                    lastFrame = rect
                }
            }
        }
    }
    
//    // MARK:- IndexPathに応じた,CollectionViewCellのレイアウトを決める
//    // Tips:- UICollectionViewLayoutAttributesに、セルサイズや位置座標のプロパティがある
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//        guard let cv = collectionView else {return nil}
//        print("*********** start layoutAttributesForItem ***********")
//        print("cv.tag :",cv.tag)
//        print("indexPath.item :",indexPath.item)
//        print("cachedAttributes :",cachedAttributes)
//        print("cv.numberOfItems :",cv.numberOfItems(inSection: 0))
//        print("*********** end layoutAttributesForItem ***********")
        
        return cachedAttributes[indexPath.item]
    }
//
//
//    // MARK:- 表示範囲内にあるセルのLayoutAtributesを返す
      // prepare()内でcachedAttributesに格納した、attributeのフレームとcontentBoundsを一致させる →検索処理が重くなりやすいので、二分探索で検索
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        // 完全一致で一度に検索すると、キャッシュが溜まってくると重くなる
//        return cachedAttributes.filter {(attributes:UICollectionViewLayoutAttributes)->Bool in
//            return rect.intersects(attributes.frame)
//        }
        
        var attributesArray = [UICollectionViewLayoutAttributes]()
        
        
        guard let lastIndex = cachedAttributes.indices.last,
              let firstMatchIndex = binarySearch(range: 0...lastIndex, rect: rect) else {return attributesArray}
        
        // 以下、二分探索の続き
        for attributes in cachedAttributes[..<firstMatchIndex].reversed() {
            // クロージャ内をreturn nilにすると、まだ要素がなかった場合にnilを返してしまう
            // →break で処理を止める
            guard attributes.frame.maxY >= rect.minY else { break }
            attributesArray.append(attributes)
        }
        
        for attributes in cachedAttributes[firstMatchIndex...]{
            guard attributes.frame.minY <= rect.maxY else {break}
            attributesArray.append(attributes)
        }
        
        return attributesArray
    }
    
    // 二分探索アルゴリズム. もっと抽象化して汎用化できそう.
    func binarySearch(range: ClosedRange<Int>, rect: CGRect) -> Int?
    {
        
        guard var left = range.first,var right = range.last else {return nil}

        while left <= right {

            let middle = Int(floor(Double(left + right) / 2.0))
            let attribute = cachedAttributes[middle]
            
            if attribute.bounds.intersects(rect){
                return middle
            } else {
                if attribute.frame.maxY < rect.minY{
                    left = middle + 1
                } else {
                    right = middle - 1
                }
            }
        }

        return nil
    }
}

